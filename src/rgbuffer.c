//! \file 
//! \addtogroup RingbufChar_sFER
//! \author Luke Paulson
//! \date June 1, 2020
//! \copyright The MIT License (MIT) 
//! \version 

#include "rgbuffer.h"

/*!
  \param size size of desired buffer
  \return pointer to RINGBUG object
*/
RingbufChar_s* create_rgbuffer_c(size_t size){
  RingbufChar_s* self = (RingbufChar_s*)malloc(sizeof(RingbufChar_s));

  if(self == NULL){
    return NULL;
  }
  
  //Checking for obsensly large buffers buffers
  if(size > MAX_BUF_LEN){
    size = MAX_BUF_LEN;
  }

  //Initilize data
  self->data = (char*)malloc(sizeof(char)*size);
  self->head = 0;
  self->tail = 0;
  self->size = size;
  self->empty  = true;
  self->full = false;
  self->buf_density = 0;

  //Attach function pointers
  //self->delete = &delete_buf_char;
  //self->write = &write_char;
  //self->write_n = &write_char_n;
  //self->read = &read_char;
  //self->read_n = &read_char_n;
  //self->flush = &flush_buf_char;
  //self->is_empty = &is_empty_char;
  //self->is_full = &is_full_char;
  //self->peek = &peek_char;
  //self->read_i = &read_char_index;
  //self->write_i = &write_char_index;

  return self;
}


void delete_rgbuffer_c(RingbufChar_s* self){
  free(self->data);
  free(self);
  return;
}

/*!
  \retval TRUE Character written
  \retval FALSE Character not written
*/
bool rgbuffer_write_c(RingbufChar_s* self, char c){
  //check that bufer is not full
  if(self->full){
    return false;
  }
  //buffer no longer empty
  self->empty = false;
  
  memcpy(self->data+self->head, &c, 1);

  //Update the buffer desity
  self->buf_density++;
  
  //check head is not past data length
  if(++self->head > self->size-1){
    self->head = 0;
  }

  //check that head is not equal tail
  if(self->head == self->tail){
    self->full = true;
  }

  return true;
}

/*!
  \retval NULL Buffer Empty
*/
char rgbuffer_read_c(RingbufChar_s* self){
  //check if buffer empty
  if(self->empty){
    return false;    
  }
  char buf;
  
  //buffer no longer full
  self->full = false;
  
  memcpy(&buf, self->data+self->tail, 1);

  //Update buffer density
  self->buf_density--;

  //check tail is not past data length
  if(++self->tail > self->size-1){
    self->tail = 0;
  }
  //check that tail not equal to head
  if(self->tail == self->head){
    self->empty = true;
  }
  
  return buf;
}

/*!
  \param self Ring Buffer Object
  \param s Pointer to array of bytes
  \param n Number of bytes to write

  \return Actual number of bytes writen
*/
//TODO: the n input can probably be removed, and the size n can be determined in the function need to lookin to this deeper one day...
uint8_t rgbuffer_write_nc(RingbufChar_s* self, char* s, size_t n){
  if(n > MAX_BUF_LEN){
    n = MAX_BUF_LEN;
  }
  uint8_t i;
  for(i = 0; i < n; i++){
    //write untill buffer full
    if(!(rgbuffer_write_c(self, s[i]))){
      return i;
    }
  }
  return i;
}

/*!
  \param self Ring Buffer Object
  \param dest Pointer to destination buffer
  \param n Number of bytes to read

  \return Number of bytes actually read
*/
uint8_t rgbuffer_read_nc(RingbufChar_s* self, char* dest, size_t n){
  char* temp_buf;
  //Check for oversized buffer
  if(n > self->size){
    n = self->size; 
  }
  //Check for -1 or read whole buffer
  else if(n < 0){
    n = self->size; 
  }
  else if (n == 0){
    return 0;
  }

  temp_buf = malloc(sizeof(char)*n);
  
  uint32_t i;
  for(i = 0; i < n; i++){
    //Check if the buffer is empty
    if(rgbuffer_empty_c(self)){
      break;
    }
    temp_buf[i] = rgbuffer_read_c(self);
  }

  
  //copy temp_buffer into destination
  if(dest != NULL){
    dest = memcpy(dest, temp_buf, n);
  }
  else{
    //Do somthing useful
  }
  
  free(temp_buf);
  return i;
}

/*!
  \param self Ring Buffer Object

  \return None
*/
void rgbuffer_flush_c(RingbufChar_s* self){
  self->empty = true;
  self->full = false;
  self->tail = self-> head;
  self->buf_density = 0;
  return;
}

/*!
  \param self Ring Buffer Object


  \return boolean full status
*/
bool rgbufffer_full_c(RingbufChar_s* self){
  return self->full;
}

/*!
  \param self Ring Buffer Object

  \return boolean empty status
*/
bool rgbuffer_empty_c(RingbufChar_s* self){
  return self->empty;
}

/*!
  \param self Ring Buffer Object

  \return Return next char without pulling from buffer
*/
char rgbuffer_peek_c(RingbufChar_s* self){
  //check if buffer empty
  if(self->empty){
    return '\0';    
  }
  char buf;
 
  memcpy(&buf, self->data+self->tail, 1);
  
  return buf;
}

/*!
  \param self Ring Buffer Object
  \param i index value to read from, with head* being 0

  \return Number of bytes actually read
*/
char rgbuffer_read_index_c(RingbufChar_s* self, uint8_t i){  
  char return_char;
 
  //Check if buffer is empty, if so nothing can be read
  if(self->empty){
    return 0;
  }

  //Check if i is greated than then number if data in the buffer
  if (i >= self->buf_density){
    return rgbuffer_read_c(self);
  }

  //Reindex i with respect to self->data, zeroth index
  //Case A: Head index is 0
  if(self->head == 0){
    i = self->size - i - 1;
  }
  /*Case B: Head index is behind Tail (rollover) and the index is less than
    head index putting it between head and the origin
  */
  else if(i < self->head){
    i = self->head - i - 1;
  }
  /* Case C: Head index is behind Tail (rollover) and the index is greater than
    the head index putting it between head and 
  */
  else{
    i = (self->head + self->size - i - 1);
  }

  //Get char byte from new index location
  memcpy(&return_char, (self->data + i), sizeof(char));


  /*shift data to fill empty space, n is one memory behind i. The data will shift forward.
  * when data+n = head, all data will have been shifted.
  */
  uint16_t n = 0;
  if(i < self->size-1){
    n = i + 1;
  }

  do{
    memmove(self->data+i, self->data+n, sizeof(char));
    n++;
    i++;

    //Check for when n and i fall outside the buffer and need to be rolled over 
    if(n >= self->size){
      n = 0;
    }
    if(i >= self->size){
      i = 0;
    }
  }while(n != self->head);

  //The new head postion need to be set
  self->head--;

  //Reduce buffer density
  self->buf_density--;

  //Unset buffer full flag
  self->full = false;

  return return_char;

}

/*!
  \param self Ring Buffer Object
  \param i index value to write to, with head* being 0
  \param c character to write

  \return Number of bytes actually read
*/
uint8_t rgbuffer_write_index_c(RingbufChar_s* self, char c, size_t i){

  //Check if buffer is full
  if(rgbufffer_full_c(self)){
    return 0;
  }

  if(i <= 0){
    return rgbuffer_write_c(self, c);
  }

  //Check if i is greated than then number if data in the buffer
  if (i > self->buf_density){
    i = self->buf_density;
  }

  //Reindex i with respect to self->data, zeroth index
  //Case A: Head index is 0
  if(self->head == 0){
    i = self->size - i;
  }
  /*Case B: Head index is behind Tail (rollover) and the index is less than
    head index putting it between head and the origin
  */
  else if(i <= self->head){
    i = self->head - i;
  }
  /* Case C: Head index is behind Tail (rollover) and the index is greater than
    the head index putting it between head and 
  */
  else{
    i = (self->head + (self->size - 1) + i);
  }

  //Shift data forward starting at head and moving back to i
  uint32_t j = self->head;
  uint32_t n = 0;

  //If head is at the end of the buffer loop j around
  if(self->head == 0){
    n = self->size - 1;
  }
  else{
    n = j-1;
  }
  
while(n != i){
  //copy data from n to j (n to n+1)
  memcpy(self->data+j, self->data+n, sizeof(char));
  n--;
  j--;
    
  if( n > (self->size - 1)){
    n = self->size - 1;
  }
  if (j > (self->size - 1)){
    j = self->size - 1;
  }
  }

//copy data from i to j (i to i+1)
memcpy(self->data+j, self->data+i, sizeof(char));
//write the character to the index position
memcpy(self->data+i, &c, sizeof(char));

//Update head location
self->head++;
if(self->head >= self->size){
  self->head = 0;
}

//Incrment the buffer density
self->buf_density++;
//check if the buffer is full
if(self->head == self->tail){
  self->full = true;
}

return true;

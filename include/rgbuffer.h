//! \file
//! \addtogroup RINGBUFFER
//! \brief Simple Ringbuffer library
//! \details 
//! \author Luke Paulson
//! \date June 1, 2020
//! \copyright The MIT License (MIT) 
//! \version 
// Adding a line to test the how submodules work

#ifndef RGBUFFER_H
#define RGBUFFER_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>

typedef struct RingbufChar_struct RingbufChar_s;

#define MAX_BUF_LEN 32        //!< Maximum buffer size

//! Ringbuffer object
struct RingbufChar_struct{
  char* data;                 //!< Pointer to data
  size_t head;                 //!< index to head of buffer
  size_t tail;                 //!< index to tail of buffer
  size_t size;                //!< Size of buffer in bytes
  bool empty;                 //!< Empty Flag - Set when tail = head
  bool full;                  //!< Full flag - Set when head = tail
  uint16_t buf_density;       //!< number of elements in buffer

};

RingbufChar_s* create_rgbuffer_c(size_t);                        //!< Create Ringbuffer
void delete_rgbuffer_c(RingbufChar_s*);                        //!< Destroy Ringbuffer
bool rgbuffer_write_c(RingbufChar_s*, char);                       //!< Write single byte
char rgbuffer_read_c(RingbufChar_s*);                              //!< Read single char
uint8_t rgbuffer_write_nc(RingbufChar_s*, char*, size_t);         //!< Write 'n' number of bytes
uint8_t rgbuffer_read_nc(RingbufChar_s*, char*, size_t);          //!< Read 'n' number of bytes
void rgbuffer_flush_c(RingbufChar_s*);                         //!< Flush the buffer
bool rgbufffer_full_c(RingbufChar_s*);                           //!< Check if buffer is full
bool rgbuffer_empty_c(RingbufChar_s*);                          //!< Check if buffer is empty
char rgbuffer_peek_c(RingbufChar_s*);                              //!< Peek at the next byte without reading
char rgbuffer_read_index_c(RingbufChar_s*, uint8_t);               //!< Read char from index position of buffer
uint8_t rgbuffer_write_index_c(RingbufChar_s*, char, size_t);     //!< Write char to index position of buffer

#endif // RGBUFFER_H
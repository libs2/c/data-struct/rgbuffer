//! \file
//! \addtogroup TEST
//! \brief Ceedling test file for rgbuffer.c
//! \details 
//! \author Luke Paulson
//! \date June 1, 2020
//! \copyright The MIT License (MIT)
//! \version  

#include "unity.h"
#include "rgbuffer.h"


RingbufChar_s* test_buffer_1;                     //!< Device under test
char* temp_buffer_p;                              //!< Pointer to temp buffer

//! Setup function called before each test
void setUp(void){                                   
  test_buffer_1 = create_rgbuffer_c(16);
  temp_buffer_p = (char*)calloc(MAX_BUF_LEN, sizeof(char));
}
//! Teardown function called after each test 
void tearDown(void){
  delete_rgbuffer_c(test_buffer_1);
  free(temp_buffer_p);
}

//! \test Write a single byte to the an empty Ring Buffer
void test_write_single_byte_to_empty_buffer(void){
    TEST_ASSERT_TRUE(rgbuffer_write_c(test_buffer_1, 'c'));
}

//! \test Write a single byte to a full buffer 
void test_write_single_byte_to_full_buffer(void){
    test_buffer_1->full = true;
    TEST_ASSERT_FALSE(rgbuffer_write_c(test_buffer_1, 'c'));
}

//! \test Read from empty buffer
void test_read_from_empty_buffer(void){
  TEST_ASSERT_EQUAL_CHAR('\0', rgbuffer_read_c(test_buffer_1));
}
//! \test Write then read a single byte
void test_write_the_read(void){
  rgbuffer_write_c(test_buffer_1, 'c');
  TEST_ASSERT_EQUAL_CHAR('c', rgbuffer_read_c(test_buffer_1));
}
//! \test Write multiple bytes
void test_write_multiple_bytes(void){
  TEST_ASSERT_EQUAL_UINT8(11, rgbuffer_write_nc(test_buffer_1, "hello world", 11));
}
//! \test Write multiple byte then read multiple bytes and test the return value 
void test_write_multiple_read_multiple_num(void){
  rgbuffer_write_nc(test_buffer_1, "hello world", 11);
  TEST_ASSERT_EQUAL_UINT8(11, rgbuffer_read_nc(test_buffer_1, temp_buffer_p, 11));
}

//! \test Write multiple byte then read multiple bytes and verify the string is correct
void test_write_multiple_read_multiple(void){
  rgbuffer_write_nc(test_buffer_1, "hello world", 11);
  TEST_ASSERT_EQUAL_UINT8(11, rgbuffer_read_nc(test_buffer_1, temp_buffer_p, 11));
  TEST_ASSERT_EQUAL_STRING("hello world", temp_buffer_p);
}
//! \test Write more than maximum buffer size
void test_write_more_than_max_buffer(void){
  TEST_ASSERT_EQUAL_UINT8(16, rgbuffer_write_nc(test_buffer_1, "massive huge string that wont fit", 33));
}
//! \test Cyclic write read
void test_cycle_read_write_sequence(void){
  TEST_ASSERT_EQUAL_UINT(10, rgbuffer_write_nc(test_buffer_1, "abcdefghij", 10));
  TEST_ASSERT_EQUAL_UINT8(5, rgbuffer_read_nc(test_buffer_1, temp_buffer_p, 5));
  TEST_ASSERT_EQUAL_UINT(10, rgbuffer_write_nc(test_buffer_1, "abcdefghij", 10));
  TEST_ASSERT_EQUAL_UINT8(5, rgbuffer_read_nc(test_buffer_1, temp_buffer_p, 5));
  TEST_ASSERT_EQUAL_UINT(6, rgbuffer_write_nc(test_buffer_1, "abcdefghij", 10));
}

//! \test Flush buffer
void test_flush_buffer(void){
  rgbuffer_write_nc(test_buffer_1, "bigbigbigbigbigbig", 18);
  TEST_ASSERT_FALSE(rgbuffer_write_c(test_buffer_1, 'c'));
  rgbuffer_flush_c(test_buffer_1);
  TEST_ASSERT_TRUE(rgbuffer_empty_c(test_buffer_1));
  TEST_ASSERT_TRUE(rgbuffer_write_c(test_buffer_1, 'c'));
}


//TODO: Look into this test that -1 dosen't make sense
//! \test Read entire buffer
void test_read_entire_buffer(void){
  rgbuffer_write_nc(test_buffer_1, "Hello world\0", 11);
  TEST_ASSERT_EQUAL_UINT8(11, rgbuffer_read_nc(test_buffer_1, temp_buffer_p, -1));
  TEST_ASSERT_EQUAL_STRING("Hello world", temp_buffer_p);
}

//! \test Test full flag
void test_full_flag(void){
  TEST_ASSERT_FALSE(rgbufffer_full_c(test_buffer_1));
  rgbuffer_write_nc(test_buffer_1, "bigbigbigbigbigbig", 18);
  TEST_ASSERT_TRUE(rgbufffer_full_c(test_buffer_1));
}

//! \test Test empty flag
void test_empty_flag(void){
  TEST_ASSERT_TRUE(rgbuffer_empty_c(test_buffer_1));
  rgbuffer_write_c(test_buffer_1, 'c');
  TEST_ASSERT_FALSE(rgbuffer_empty_c(test_buffer_1));
}

//! \test Peek at buffer without removing char
void test_buffer_peek(void){
  rgbuffer_write_nc(test_buffer_1, "hello", 5);
  TEST_ASSERT_EQUAL_CHAR(rgbuffer_peek_c(test_buffer_1), 'h');
  TEST_ASSERT_EQUAL_CHAR(rgbuffer_read_c(test_buffer_1), 'h');
}

//! \test Mid buffer read fail - empty
void test_mid_buffer_read_empty_fail(void){
  //test if read_index from empty bufer
  TEST_ASSERT_FALSE(rgbuffer_read_index_c(test_buffer_1, 3));
}

//! \test Mid buffer read
void test_mid_buffer_read(void){
  rgbuffer_write_nc(test_buffer_1, "hello", 5);
  TEST_ASSERT_EQUAL_CHAR(rgbuffer_read_index_c(test_buffer_1, 3), 'e');
  TEST_ASSERT_EQUAL_CHAR(rgbuffer_read_index_c(test_buffer_1, 3), 'h');
  TEST_ASSERT_EQUAL_UINT8(3, rgbuffer_read_nc(test_buffer_1, temp_buffer_p, 3));
  TEST_ASSERT_EQUAL_STRING("llo", temp_buffer_p);
}

//! \test Mid buffer read set empty flag
void test_mid_buffer_read_set_empty(void){
  TEST_ASSERT_TRUE(rgbuffer_empty_c(test_buffer_1));
  rgbuffer_write_c(test_buffer_1,'c');
  TEST_ASSERT_FALSE(rgbuffer_empty_c(test_buffer_1));
  TEST_ASSERT_EQUAL('c',rgbuffer_read_index_c(test_buffer_1, 1));
  TEST_ASSERT_TRUE(rgbuffer_empty_c(test_buffer_1));
}

//! \test Mid buffer read clear full flag
void test_mid_buffer_read_clear_full(void){
  rgbuffer_write_nc(test_buffer_1,"bigbigbigbigbigb", 16);
  TEST_ASSERT_TRUE(rgbufffer_full_c(test_buffer_1));
  TEST_ASSERT_EQUAL('g', rgbuffer_read_index_c(test_buffer_1, 1));
  TEST_ASSERT_FALSE(rgbufffer_full_c(test_buffer_1));
}

//! \test Mid buffer read index overflow - read from tail
void test_mid_buffer_read_index_overflow(void){
  rgbuffer_write_nc(test_buffer_1,"small", 5);
  TEST_ASSERT_EQUAL('s', rgbuffer_read_index_c(test_buffer_1, 10));
}

//! \test Mid buffer read index index 0 - read from head-1
void test_mid_buffer_read_index_zero(void){
  rgbuffer_write_nc(test_buffer_1,"Hello", 5);
  TEST_ASSERT_EQUAL('o', rgbuffer_read_index_c(test_buffer_1, 0));
}


//! \test Mid Buffer write fail - full
void test_mid_bufer_write_full_failure(void){
  //test if write_index to full buffer
  test_buffer_1->full = true;
  TEST_ASSERT_FALSE(rgbuffer_write_index_c(test_buffer_1, 1, 'c'));
}

//! \test Mid Buffer write
void test_mid_bufer_write(void){
  rgbuffer_write_nc(test_buffer_1, "Hello", 5);
  rgbuffer_write_index_c(test_buffer_1, 'd', 1);
  rgbuffer_read_nc(test_buffer_1, temp_buffer_p, -1);
  TEST_ASSERT_EQUAL_STRING("Helldo", temp_buffer_p );
}

//! \test Mid Buffer write set full Flag
void test_mid_bufer_write_set_full(void){
  rgbuffer_write_nc(test_buffer_1,"foofoofoofoofoo", 15);
  TEST_ASSERT_FALSE(rgbufffer_full_c(test_buffer_1));
  rgbuffer_write_index_c(test_buffer_1, 'c', 1);
  TEST_ASSERT_TRUE(rgbufffer_full_c(test_buffer_1));

}

//! \test mid buffer write index overflow - write to tail
void test_mid_write_index_overflow(void){
  rgbuffer_write_nc(test_buffer_1,"midmidmid", 9);
  rgbuffer_write_index_c(test_buffer_1, 'c', 15);
  rgbuffer_read_nc(test_buffer_1, temp_buffer_p, -1);
  TEST_ASSERT_EQUAL_STRING("cmidmidmid", temp_buffer_p );
}

//! \test Mid buffer write index index 0 - write to head
void test_mid_buffer_write_index_zero(void){
  rgbuffer_write_nc(test_buffer_1,"midmidmid", 9);
  rgbuffer_write_index_c(test_buffer_1, 'c', 0);
  rgbuffer_read_nc(test_buffer_1, temp_buffer_p, -1);
  TEST_ASSERT_EQUAL_STRING("midmidmidc", temp_buffer_p );
}